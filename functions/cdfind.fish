
function cdfind -d "find a directory from . and cd to it"
    set dir (fd $argv --type directory | fzf --header="cd to directory")
    test -n $dir && cd $dir
    commandline -f repaint
end

