
#TODO: make a nicer lockscreen with i3lock, see https://github.com/betterlockscreen/betterlockscreen

function _i3lock
    # we need to do some voodoo to ensure it doesn't use the same seed every time
    set seed (date | cksum)
    random (string split ' ' $seed)[1]
    set pic (random choice $HOME/Pictures/bg/**.png)
    i3lock -c 000000 -t -i $pic
end

function fexit -d 'command line utility for exiting, suspending or locking the computer'
    if test (cat /proc/1/comm) = "systemd"
        set logind systemctl
    else
        set logind loginctl
    end

    argparse -n fexit 'l/nolock' -- $argv

    switch $argv[1]
        case lock
            _i3lock
        case suspend
            set -q _flag_nolock || _i3lock
            $logind suspend
        case hibernate
            set -q _flag_nolock || _i3lock
            $logind hibernate
        case reboot
            $logind reboot
        case shutdown
            $logind poweroff
        case '*'
            echo '◖ fexit: missing or invalid argument ◗'
            return 2
    end

    return 0
end
