
function __rmlatex_file_extensions
    set ext \
        aux \
        bbl \
        blg \
        fdb_latexmk \
        fls \
        synctex.gz \
        log \
        out \
        tdo \
        toc

    for ex in $ext
        echo $ex
    end
end

function rmlatex -a name -d 'delete the endless crap generated during latex compilation'
    argparse -n rmlatex -- $argv
    
    for ext in (__rmlatex_file_extensions)
        set fname $name.$ext
        rm -fv $fname
    end
end
