
#TODO: see new docs on generating keys here
# https://github.com/debauchee/barrier-wiki/blob/f5f2751243f2af084f42ed5b50d676b538f6d81e/Command-Line.md#ssl_config

function __barrier_gen_key
    set dir $HOME/.local/share/barrier/SSL

    test -d $dir || mkdir -p $dir

    openssl req -x509 -nodes -days 365 -subj /CN=Barrier \
        -newkey rsa:2048 \
        -keyout $dir/Barrier.pem \
        -out $dir/Barrier.pem

    test -d $dir/Fingerprints || mkdir -p $dir/Fingerprints
    rm -f $dir/Fingerprints/Local.txt
    set hash (openssl x509 -fingerprint -sha256 -noout -in $dir/Barrier.pem)
    string split -f 2 '=' $hash > $dir/Fingerprints/Local.txt

    echo (set_color blue)"barrier:"(set_color normal)" generated key pair.
    You will need to copy `~/.local/barrier/SSL/Fingerprints/Local.txt` to
    `~/.local/barrier/SSL/Fingerprints/TrustedServers.txt` on the client.
    Might still be FUBAR, the GUI does something mysterious and annoying to
    fix it, so try that.
    "
end

function __barrier_help
    echo "\
    "(set_color green)"barrier"(set_color normal)" - simple helper utility for using `barrier`

    See the barrier repo: https://github.com/debauchee/barrier

    "(set_color blue)"Usage:"(set_color normal)"
      barrier [client|server] start [args...]
      barrier [client|server] stop [args...]
      barrier genkey

    "(set_color blue)"Options:"(set_color normal)"
      -s,--server <server_ip>        Server for the client to connect to.
      -c,--config <config_file>      Server config file to use.
      --enable-client-cert-checking  We change default since it's currently fucked. Server only.

    All other options are forwarded to the `barrierc` or `barriers` commands.

    "(set_color blue)"Subcommands:"(set_color normal)"
      start    Start the barrier daemon.
      stop     Stop the barrier daemon.

    The barrier server config file can be set with `BARRIER_CONFIG`.  If not set, it will default to
    `\$HOME/.config/Debauchee/server.conf`.

    For clients, the server to connect to can be set with `BARRIER_SERVER` and must be set (or passed
    with `-s,--server`).

    When using for the first time you will need to run `barrier genkey` to generate a key pair.
    That process may still be somewhat fucked up, follow the instructions it gives you.
    "
end

function __barrier_client -a com
    argparse -i "s/server" -- $argv

    set argv $argv[2..-1]

    if test $com = "stop"
        pkill barrierc
        return
    end

    if ! set -q _flag_s
        if not set -q BARRIER_SERVER
            echo (set_color red)"error:"(set_color normal)" must either set BARRIER_SERVER variable or pass
            `-s,--server` argument"
            return 1
        end
        set _flag_s $BARRIER_SERVER
    end

    if test $com = "start"
        if pgrep "barrierc"
            echo (set_color red)"error:"(set_color normal)" barrierc already running"
            return 1
        end
        eval "barrierc $argv $_flag_s"
    else
        echo (set_color red)"error:"(set_color normal)" unrecongized command $com"
        return 1
    end
end

function __barrier_server -a com
    argparse -i "c/config" "enable-client-cert-checking" -- $argv

    set argv $argv[2..-1]

    set -q _flag_enable-client-cert-checking || set _certcheck "--disable-client-cert-checking"

    if test $com = "stop"
        pkill barriers
        return
    end

    if not set -q _flag_c
        if set -q BARRIER_CONFIG
            set _flag_c $BARRIER_CONFIG
        else
            set _flag_c "$HOME/.config/Debauchee/server.conf"
        end
        if ! test -e $_flag_c
            echo (set_color red)"error:"(set_color normal)" no barrier config file at $_flag_c"
            return 1
        end
    end

    if test $com = "start"
        if pgrep "barriers"
            echo (set_color red)"error:"(set_color normal)" barriers already running"
            return 1
        end
        eval "barriers -c $_flag_c $_certcheck $argv"
    else
        echo (set_color red)"error:"(set_color normal)" unrecognized command $com"
    end
end

function barrier -a cs com
    argparse -i -n barrier "h/help" -- $argv

    if set -q _flag_h
        __barrier_help
        return
    end

    set argv $argv[3..-1]

    if test $cs = genkey
        __barrier_gen_key $com || return 1
    else if test $cs = client -o $cs = c
        __barrier_client $com $argv || return 1
    else if test $cs = server -o $cs = s
        __barrier_server $com $argv || return 1
    else
        echo (set_color red)"error:"(set_color normal)" first argument must be one of client,c,server,s"
        return 1
    end
end

