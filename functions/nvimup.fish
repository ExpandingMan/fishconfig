function __nvimup_packersync
    echo (set_color magenta)"    Updating neovim plugins..."(set_color normal)
    nvim --headless -c 'autocmd User PackerComplete quitall' -c PackerSync
    echo (set_color green)"      done!"(set_color normal)
end

function __nvimup_download_appimage
    echo (set_color magenta)"    Downloading neovim "$argv[2]" binaries..."(set_color normal)
    rm -f $HOME/sbin/nvim
    wget $argv[1] -O $HOME/sbin/nvim
    chmod a+x $HOME/sbin/nvim
    echo (set_color green)"      done!"(set_color normal)
end

function __nvimup_config
    echo (set_color magenta)"    Updating neovim config repo..."(set_color normal)
    git -C $HOME/.config/nvim pull origin master
    echo (set_color green)"      done!"(set_color normal)
end

function nvimup -d 'simple utility for updating neovim'
    set nightly_url "https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage"
    set latest_url "https://github.com/neovim/neovim/releases/download/v0.10.1/nvim.appimage"

    argparse -n nvimup 'h/help' 's/sync' 'c/config' -- $argv

    if set -q _flag_h
        #TODO: create a help-string maker
        echo -e "neovim updater\n\n"
        return
    end

    # default behavior is only to do packer update
    if test (count $argv) = 0
        __nvimup_config && __nvimup_packersync
        return 0
    end

    switch $argv
        case nightly
            __nvimup_download_appimage $nightly_url nightly
        case latest
            __nvimup_download_appimage $latest_url latest
        case config
            __nvimup_config
        case '*'
            echo (set_color red)"unrecognized arguments $argv"
            return 1
    end

    set -q _flag_c && __nvimup_config
    set -q _flag_s && __nvimup_packersync
end

