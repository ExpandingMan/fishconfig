
function editfind -d "interactively search for a file to edit"
    set fname (fd --type file | fzf --header="edit file")
    if not test -z $fname
        for a in $argv
            # want option to change dir *first*, but need to modify filename argument
            if test $a = "--cd" || test $a = "-c"
                cd (dirname $fname)
                $EDITOR (basename $fname)
                commandline -f repaint
                return 0
            end
        end
        $EDITOR $fname
    end
end

