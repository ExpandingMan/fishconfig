function __rhist_delete
    set o (history | fzf --header="delete history entry")
    history delete --exact --case-sensitive "$o"
    if not test -z $o
        echo (set_color blue)▮ (set_color magenta)deleted history "entry (not instantaneous):" (set_color red)$o(set_color normal)
    end
end

function rhist -d "reverse history search"
    argparse -n nvimup 'd/delete' -- $argv

    set -q _flag_d && __rhist_delete && return

    commandline -i (history | fzf --header="reverse history search")
    commandline -f repaint
end
