
function tuner -d 'play a tone for tuning an instrument'
    if test (count $argv) -ge 1
        set freq $argv[1]
    else
        set freq 440
    end
    speaker-test -t sine -f $freq
end
