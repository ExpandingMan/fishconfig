

function arxivget -d 'download paper from arxiv'
    set url 'https://arxiv.org/pdf/'

    argparse -n arxivget 'h/help' -- $argv

    if set -q _flag_h
        echo -e "arxivget ID [TITLE]"
        return
    end

    set pdfurl "$url$argv[1].pdf"

    if test (count $argv) = 0
        echo "ERROR: arxivget requires at least 1 argument"
        return
    end

    if test (count $argv) -gt 1
        set output "-O$argv[2]"
    else
        set output ''
    end

    wget $pdfurl $output

    return 0
end
