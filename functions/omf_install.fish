function omf_install -d "installs oh-my-fish"
    set url 'https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install'
    curl -L $url | fish
end
