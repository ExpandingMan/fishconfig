function starship_multiline_toggle -d "toggle starship to use alternate, multi-line config file"
    set multiline "$HOME/.config/starship-multiline.toml"

    # only do this if the file exists
    if test -e $multiline
        if test -n "$STARSHIP_CONFIG"
            set -e STARSHIP_CONFIG
        else
            set -gx STARSHIP_CONFIG $multiline
        end
    end

    commandline -f repaint

    return 0
end

