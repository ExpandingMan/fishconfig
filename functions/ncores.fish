function ncores -d "returns the number of cores on the machine"
    lscpu -b -p=Core,Socket | grep -v '^#' | sort -u | wc -l
end
