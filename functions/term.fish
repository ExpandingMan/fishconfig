
function term -d 'guess the correct terminal window type and open it'
    if set -q TERMINAL
        exec $TERMINAL
    end

    if type -q kitty
        exec kitty
    else if type -q alacritty
        exec alacritty
    else if type -q wezterm
        exec wezterm
    else if type -q urxvt
        exec urxvt
    end
end
