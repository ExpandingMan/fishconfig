function __zhist_delete
    set o (zoxide query -ls | fzf --header="delete zoxide history entry")
    set o (string split -r -n -m1 ' ' $o)
    zoxide remove $o[-1]
end

function zhist -d "zoxide history search"
    argparse -n zhist 'd/delete' -- $argv

    set -q _flag_d && __zhist_delete && return

    set o (zoxide query -ls | fzf --header="go to directory (zoxide)")
    set o (string split -r -n -m1 ' ' $o)
    z $o[-1]
    commandline -f repaint
end

