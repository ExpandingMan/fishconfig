
function __wallyup_download
    echo (set_color magenta)"    Downloading latest moonlander firmware..."(set_color normal)
    wget $argv[1] -O $HOME/.config/moonlander.bin
    echo (set_color green)"    done!"(set_color normal)
end

function __wallyup_install
    echo (set_color magenta)"    Installing firmware..."(set_color normal)
    wally-cli $HOME/.config/moonlander.bin
    echo (set_color green)"    done!"(set_color normal)
end

function wallyup -d 'download and install latest update for moonlander'
    # the NpwY6 identifier is for my zsa.io user
    set url 'https://oryx.zsa.io/NpwY6/latest/binary'

    argparse -n wallyup 'h/help' 'd/download-only' 'i/install-only' -- $argv

    if set -q _flag_h
        #TODO: help-string maker
        echo -e "moonlander firmware updater\n\n"
        return
    end

    set -q _flag_i || __wallyup_download $url

    set -q _flag_d || __wallyup_install
end

