function bashsource -d "source a bash script"
    exec bash -c "source $argv[1]; exec fish"
end
