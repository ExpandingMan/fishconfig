
function __dotf_there
    set p (path normalize (relpath $HOME $argv))
    cp -r $HOME/$p $DOTFILES_HOME/$p
end

function __dotf_here
    set p (path normalize (relpath $HOME $argv))
    cp -r $DOTFILES_HOME/$p $HOME/$p
end

function dotf -d 'quick, simple management of dotfiles from fish'
    argparse -n dotf 'h/help' -- $argv

    if set -q _flag_h
        echo -e "dotf: simple dotfile management"
        echo -e "  - here: from dotfiles to HOME"
        echo -e "  - there: from HOME to dotfiles"
    end

    switch $argv[1]
        case there
            __dotf_there $argv[2]
        case here
            __dotf_here $argv[2]
        case '*'
            echo "invalid command $argv[2]"
            return 1
    end
end
