# Expanding Man's `fish` Config

This is a configuration for `fish`.

See [the dotfiles repo](https://gitlab.com/ExpandingMan/dotfiles) for installation.

## Resources
- [`oh-my-fish`](https://github.com/oh-my-fish/oh-my-fish) is the plugin manager I'm currently
    using.
- [Fish Cookbook](https://github.com/jorgebucaran/cookbook.fish) is a helpful guide on how to do
    stuff in `fish`.
- [`awsm.fish`](https://github.com/jorgebucaran/awsm.fish) is a small list of plugins for `fish`.
- [fish shell scripting manual](https://developerlife.com/2021/01/19/fish-scripting-manual/)

## Configuration Files
The file `/config.fish` is ignored by git and is intended for machine-local configuration.  Note
that this file is run *last* in `fish`'s loading process.

The file `/conf.d/0.fish` is so named because `fish` runs the files in `/conf.d` in lexigraphic
order, so this name ensures that `0.fish` will always be run *first*.
