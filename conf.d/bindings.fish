# key bindings

# for the below; there are just way too many presets to sift through
function _non_preset_bindings
    set bs (bind)
    for b in $bs
        string match -v '*--preset*' $b
    end
end

function prompt_reset -d "use after key bindings to fix prompt"
    echo -e "\e" && fish_prompt
end

# not really a forward search here so just do same binding for both
bind / rhist
bind \? rhist

# some consistency with my i3 binding
bind \cY "bind | fzf"
bind -M insert \cY "bind | fzf"

# <space><space> write a command in nvim
bind \x20\x20 edit_command_buffer

#TODO: for some reason this only clears for one line
bind -M insert \cX clear

# copy current directory to clipboard
bind yy "pwd | xc -r"
#TODO: this is very wonky for reasons I don't understand
bind pp "xc -o"

# more general search bindings with leader
bind \\h rhist
bind \\d "rhist -d"
bind \\k "_non_preset_bindings | fzf"
bind \\f editfind
bind \\F "editfind --cd"
bind \\c cdfind
bind \\b "functions | fzf | source"
bind \\z zhist
bind \\Z "zhist -d"
bind g "yy; fish_prompt"

# starship prompt toggles; \x20 is <space>
bind \x20m starship_multiline_toggle
