
alias rm "rm -i"

alias dockerterm "docker run -it --rm"

alias install $HOME/dotfiles/install

alias g git

alias j "julia --banner=no --depwarn=yes"
alias jv "julia -i -e 'using VimBindings'"

# by sourcing ranger the shell will follow it
alias ranger "bashsource ranger"

# start Julia in a screen session
alias sjulia 'screen -s julia'

if type -q lsd
    alias ls "lsd -X"
    alias tree "lsd --tree"
end

alias ll "ls -lh"

alias mnt "udiskie-mount -a"
alias umnt "udiskie-umount -a"

if type -q bat
    alias cat bat
    set -gx MANPAGER "bat --style=grid"
    set -gx MANLESS "bat --style=grid"
end

if type -q zoxide
    alias zr "zoxide remove"
    alias zri "zoxide remove -i"
    alias zq "zoxide query -ls"
end

if type -q xclip
    alias xc "xclip -selection clipboard"
end

# this is mostly just so I don't forget this
alias weather "curl wttr.in"
alias weatherplot "curl 'v2d.wttr.in'"
