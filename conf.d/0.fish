# 0.fish
# this is the first file that fish sources and is roughly analogous to ~/.profile

set fish_greeting

# the below should be the same as what's in .profile
set -gx PATH \
    "$HOME/sbin" \
    "$HOME/.cargo/bin" \
    "$HOME/.cabal/bin" \
    "$HOME/.ghcup/bin" \
    "$HOME/.go/bin" \
    $PATH

set -gx JULIA_EDITOR nvim
set -gx EDITOR nvim
set -gx VISUAL nvim
set -gx BROWSER brave

set -gx GOPATH "$HOME/.go"

# used by dotf function
set -gx DOTFILES_HOME $HOME/dotfiles/home

if type -q bat
    set -gx PAGER bat
end

if status is-interactive
    # Commands to run in interactive sessions can go here
end

# disable greeting
set fish_greeting

# fzf and rg defaults
set -gx FZF_DEFAULT_OPTS '--prompt="▯◗ " --pointer="◗" --color=16'

#socket for nvim remote
set -gx NVIM_LISTEN_ADDRESS /tmp/nvimsocket

# enable vi mode
fish_vi_key_bindings

# cursor shapes
set -gx fish_cursor_default block
set -gx fish_cursor_insert line blink
set -gx fish_cursor_visual block
set -gx fish_cursor_replace_one underscore
set -gx fish_vi_force_cursor 1

if type -q zoxide
    zoxide init fish | source
end

if type -q starship
    starship init fish | source
end
